/*
 * #%L
 * BSD implementations of Bio-Formats readers and writers
 * %%
 * Copyright (C) 2005 - 2017 Open Microscopy Environment:
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 *   - University of Dundee
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

package loci.formats.in;

import loci.common.DataTools;
import loci.common.Location;
import loci.formats.ClassList;
import loci.formats.CoreMetadata;
import loci.formats.FormatException;
import loci.formats.FormatTools;
import loci.formats.IFormatReader;
import loci.formats.ImageReader;
import loci.formats.ReaderWrapper;
import loci.formats.WrappedReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A reader designed for use with opaque remote resources that should not be permanently fetched
 * The urlreader file represents the fileset, individual files are not available to clients
 *
 * @see loci.formats.WrappedReader
 */
public class URLReader extends WrappedReader {

  // -- Fields --
  private static final Logger LOGGER = LoggerFactory.getLogger(URLReader.class);
  private ReaderWrapper helper;

  protected final static Pattern IS_ABSOLUTE_URL = Pattern.compile("([\\p{Alnum}\\+]+)://[^/].*");

  // -- Constructor --

  /**
   * Partially constructs a new URLReader.
   * Subclasses must call initHelper() in their constructor
   */
  protected URLReader(String format, String[] suffixes) {
    super(format, suffixes);
  }

  /** Constructs a new URLReader. */
  public URLReader() {
    super("URL Reader", new String[]{"urlreader", "url.json"});
  }

  /** Initialise the helper with a list of readers */
  protected void initHelper(ClassList<IFormatReader> newClasses) {
    class RemoteReader extends ReaderWrapper {
      public RemoteReader(IFormatReader r) {
        super(r);
      }
    };

    if (newClasses == null) {
      newClasses = ImageReader.getDefaultReaderClasses();
    }

    for (Class<? extends IFormatReader> c : newClasses.getClasses()) {
      LOGGER.trace("urlreader helper: {}", c);
    }
    helper = new RemoteReader(new ImageReader(newClasses));
    callDeferredSetters(helper);
  }

  // -- WrappedReader methods --

  @Override
  protected ReaderWrapper getHelper() {
    FormatTools.assertId(currentId, true, 1);
    return helper;
  }

  // -- IFormatReader methods --

  @Override
  public void close(boolean fileOnly) throws IOException {
    if (helper != null) {
      helper.close(fileOnly);
    }
  }

  @Override
  public void close() throws IOException {
    if (helper != null) {
      helper.close();
    }
  }

  @Override
  public byte[][] get8BitLookupTable() throws FormatException, IOException {
    if (getCurrentFile() == null) {
      return null;
    }
    return helper.get8BitLookupTable();
  }

  @Override
  public short[][] get16BitLookupTable() throws FormatException, IOException {
    if (getCurrentFile() == null) {
      return null;
    }
    return helper.get16BitLookupTable();
  }

  @Override
  public String[] getSeriesUsedFiles(boolean noPixels) {
    if (noPixels) {
      return new String[] {currentId};
    }
    String[] helperFiles = helper.getSeriesUsedFiles(noPixels);
    String[] allFiles = new String[helperFiles.length + 1];
    allFiles[0] = currentId;
    System.arraycopy(helperFiles, 0, allFiles, 1, helperFiles.length);
    return allFiles;
  }

  @Override
  public String[] getUsedFiles(boolean noPixels) {
    return new String[] {currentId};
  }

  @Override
  public List<CoreMetadata> getCoreMetadataList() {
    // Only used for determining the object type.
    List<CoreMetadata> oldcore = helper.getCoreMetadataList();
    List<CoreMetadata> newcore = new ArrayList<CoreMetadata>();

    for (int s=0; s<oldcore.size(); s++) {
      CoreMetadata newMeta = oldcore.get(s).clone(this, s);
      newMeta.resolutionCount = oldcore.get(s).resolutionCount;
      newcore.add(newMeta);
    }

    return newcore;
  }

  @Override
  public boolean isSingleFile(String id) throws FormatException, IOException {
    return false;
  }

  @Override
  public boolean hasCompanionFiles() {
    return true;
  }

  // -- Internal FormatReader methods --

  /* @see loci.formats.FormatReader#initFile(String) */
  @Override
  protected void initFile(String id) throws FormatException, IOException {
    // read the URL from the file
    // the file should be either:
    // - a single line with the absolute URL only
    // - a json object of the form
    //   { "url": "scheme://host/path", "readers": ["reader.class.name", ...]}

    currentId = new Location(id).getAbsolutePath();

    JsonObject jobj;
    String url;
    final ArrayList<String> readers = new ArrayList<>();
    String content = DataTools.readFile(id).trim();
    try {
      jobj = Json.createReader(new StringReader(content)).readObject();
      System.out.println(jobj);
      url = jobj.getString("url");
      JsonArray arr = jobj.getJsonArray("readers");
      if (arr != null) {
        for (int i = 0; i < arr.size(); ++i) {
          readers.add(arr.getString(i));
        };
      }
    }
    catch (JsonParsingException e) {
      url = content;
    }
    catch (ClassCastException | NullPointerException e) {
      throw new FormatException("Invalid \"url\" or \"readers\" in JSON:" + content);
    }

    LOGGER.trace("urlreader input file: {}", currentId);
    LOGGER.trace("urlreader url: {}", url);

    if (!IS_ABSOLUTE_URL.matcher(url).matches()) {
      throw new FormatException("Expected absolute URL:" + url);
    }

    ClassList newClasses = null;
    if (!readers.isEmpty()) {
      newClasses = new ClassList<>(IFormatReader.class);
      for (String reader: readers) {
        LOGGER.trace("urlreader reader: {}", reader);
        newClasses.parseLine(reader);
      }
    }
    initHelper(newClasses);

    helper.setId(url);
    core = helper.getCoreMetadataList();
  }

}

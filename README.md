# Bio-Formats URLReader

External Bio-Formats reader for wrapping URLs in OMERO.

This is intended to allow the importing of URLs into OMERO without the remote file existing on local disk.

This reader will handle files with extensions `.urlreader` or `.url.json`.

These files may contain either:
- A single line consisting of an absolute URL
- A JSON object with fields:
  - `url`: The absolute URL, required
  - `readers`: Array of name of fully qualified reader classes to be used for reading the URL, optional


More information
----------------

For more information, see the [Bio-Formats web
site](https://www.openmicroscopy.org/bio-formats/).